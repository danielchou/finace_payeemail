using System;
using System.Collections.Generic;
using System.Text;
using Excel;
using System.Data;
using System.Windows.Forms;

namespace Payroll_System {
    class Finace_Excel : InternalLibrary.baseExport2Excel {
        string Payee, Email, Amount, Description;

        private string obj2str(object obj) {
            string reval = "";
            if (obj != null) {
                reval = obj.ToString();
            }
            return reval;
        }


        public System.Data.DataTable readData()
        {
            string ss = "";
            object[,] obj;
            base.ScanEOFLine = 2; //start scan line 
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Clear();
            dt.Columns.Clear();
            dt.Columns.Add("Payee");
            dt.Columns.Add("Email");
            dt.Columns.Add("Amount");
            dt.Columns.Add("Description");

            while (base.EOF)
            {
                obj = (object[,])base.readLine(19);
                if (obj == null) break;

                Payee = obj2str(obj[1, 1]);
                Email = obj2str(obj[1, 2]);
                Amount = obj2str(obj[1, 3]);
                Description = obj2str(obj[1, 4]);
                //---------------------------------------------------------
                ss+=string.Format("{0},{1},{2},{3}\n",
                        Payee, Email, Amount, Description);
                
                object[] o = { Payee, Email, Amount, Description };
                dt.Rows.Add(o);
            }
            return dt;
        }

        public void outPayrollList(System.Windows.Forms.DataGridView grid) {            
            int i,j;
            int _CurrentRow;
            int _ColCount = grid.Columns.Count;
            int _RowCount = grid.Rows.Count;
            string Str;
            base.SaveExcelFile_II();

            //head
            _CurrentRow = 1;
            for (i = 1; i <= _ColCount; i++) {
                Str = grid.Columns[i-1].HeaderText;
                this.Rpt_WorkSheet.Cells[_CurrentRow, i] = Str;
            }

            //content
            _CurrentRow = 2;
            for (j = 1; j <= _RowCount; j++) {
                for (i = 1; i <= _ColCount; i++) {
                    Str = ((grid.Rows[j - 1].Cells[i - 1].Value == null) ? "" : grid.Rows[j - 1].Cells[i - 1].Value.ToString());
                    this.Rpt_WorkSheet.Cells[_CurrentRow, i] = Str;
                }
                _CurrentRow++;
            }
            
  
   
        }

    }
}
