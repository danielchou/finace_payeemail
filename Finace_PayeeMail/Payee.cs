﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Data.OleDb;
using InternalLibrary;
using Payroll_System;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Configuration;
using aspxtemplate;

namespace Finace_PayeeMail {
    public partial class Payee : Form
    {
        DataTable dt = new DataTable();
        DataView dv = new DataView();
        TemplateParser tp;
        string TemplatePath
            , NewTemplatePath
            , SheetName
            , IsSend2Tester
            , IsRemoveCssCode
            , MailTester;

        public Payee()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LabMailServer.Text = ConfigurationSettings.AppSettings["EmailServer"].ToString();
            txtSubject.Text = ConfigurationSettings.AppSettings["EmailSubject"].ToString();
            txtMailer.Text = ConfigurationSettings.AppSettings["EmailSender"].ToString();
            TemplatePath = Application.StartupPath
                            + ConfigurationSettings.AppSettings["DefaultEmailTemplateFile"].ToString();
            SheetName = ConfigurationSettings.AppSettings["SheetName"].ToString();
            MailTester = ConfigurationSettings.AppSettings["Send2Tester"].ToString();
            IsRemoveCssCode = ConfigurationSettings.AppSettings["IsRemoveCssCode"].ToString();

            txtExcelSheetName.Text = SheetName;
            txtMailTester.Text = MailTester;
            ReloadTemplateParser();
        }

        public TemplateParser ReloadTemplateParser() 
        {
            tp = new TemplateParser();  //--- Initialiazed PageParser ---
            tp.SetTemplate(ReadTemplate(TemplatePath));
            this.webBrowser1.Navigate(TemplatePath);
            //string CssPart,BodyPart;
            //if (IsRemoveCssCode == "Y") { 
            //    //TrimCss=tp.getoutput
            //}
            this.txtContent.Text = tp.getoutput;
            return tp;
        }

        /// <summary>
        /// 測試用
        /// </summary>
        private void btnSendMailer_Click(object sender, EventArgs e)
        {
            IsSend2Tester = "Y";
            MessageBox.Show(BindingEmailContent2());
        }

        /// <summary>
        /// 正式發信
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            IsSend2Tester = "N";
            MessageBox.Show(BindingEmailContent2());
        }

        /// <summary>
        /// 1.Test PageParser
        /// 2.Write template as *.html file into a folder.
        /// 3.Editable.
        /// </summary>
        private string BindingEmailContent2()
        {
            string NextPayee = "", Payee, Email, Amount, TotalAmount, Dscr
                    //, SendDate = DateTime.Now.ToString("MMM d,yyyy", new CultureInfo("en-US"))
                    , SendDate = this.txtSendDate.Text.Trim()
                    , dvSort = ConfigurationSettings.AppSettings["DataGridViewSort"].ToString();
            int k = 0;
            decimal dAmount = 0, dSumAmount = 0;
            StringBuilder sb = new StringBuilder();

            try
            {
                dv = dt.DefaultView;
                dv.Sort = dvSort;
                dt = dv.ToTable();
            }
            catch (Exception ex) 
            {
                string msg = "請確認EXCEL匯入是否有異常?\n\n{0}";
                return string.Format(msg, ex.ToString());
            }

            try
            {
                if(string.IsNullOrEmpty(SendDate)) {
                    return "請填上{{SendDate}}後再按發信一次，謝謝!!";
                }

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    NextPayee = (i == dt.Rows.Count - 1) ? "" : dt.Rows[i + 1][0].ToString();
                    Payee = dt.Rows[i][0].ToString();
                    Email = dt.Rows[i][1].ToString();
                    Amount = dt.Rows[i][2].ToString();
                    Dscr = dt.Rows[i][3].ToString();

                    dAmount = System.Convert.ToDecimal(Amount);
                    Amount = dAmount.ToString("N0");
                    dSumAmount += dAmount;
                    TotalAmount = dSumAmount.ToString("N0");

                    tp=ReloadTemplateParser();                  // ---Starting TemplateParser.                
                    tp.UpdateBlock("PayeeDetails");
                        tp.SetVariable("Payee", Payee);
                        tp.SetVariable("Amount", Amount);
                        tp.SetVariable("Dscr", Dscr);
                        //tp.ParseBlock("PayeeDetails");             // --- 這若補上會重複出現資料 ---
                        sb.Append(tp.getblock("PayeeDetails"));     // --- Just get specific block.
  
                    if (Payee == NextPayee)
                    {
                        k += 1;
                    }
                    else
                    {
                        tp.SetVariable("Payee",Payee);
                        tp.SetVariable("SendDate",SendDate);
                        tp.SetVariable("TotalAmount",TotalAmount);
                        tp.SetVariable("LayoutPayeeDetail", sb.ToString());
                        Email = (IsSend2Tester == "Y") 
                            ? ConfigurationSettings.AppSettings["Send2Tester"].ToString() : Email;
                        
                        paySEND(Email, tp.getoutput.Trim());

                        k = 0;        // --- Reset Index ---
                        dSumAmount = 0;
                        sb = new StringBuilder();
                    }
                }

                return "Mails send successfully.";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private string ReadTemplate(string filapath)
        {
            string value = null;
            if (filapath.Length > 0)
            {
                if (File.Exists(filapath))
                {
                    StreamReader reader = new StreamReader(filapath);
                    value = reader.ReadToEnd();
                    reader.Close();
                }
            }
            return value;
        }

        private MailMessage SendMail(string Subject,string From, string To, string Body)
        {

            MailMessage m = new MailMessage();
            m.SubjectEncoding = Encoding.UTF8;
            m.BodyEncoding = Encoding.UTF8;
            m.IsBodyHtml = true;
            SmtpClient smtpcl = new SmtpClient();
            smtpcl.Host = LabMailServer.Text.Trim();

            m.Subject = Subject;
            m.From = new MailAddress(From);
            m.To.Add(new MailAddress(To));
            m.Bcc.Add(new MailAddress("daniel.chou@onelab.tw"));
            m.Body = Body;
            smtpcl.Send(m);
            return m;
        }


        private void paySEND(string recipient, string EmailBody)
        {
            MailMessage m=new MailMessage();
            try
            {
                m=SendMail(txtSubject.Text.Trim()
                            , txtMailer.Text.Trim()
                            , recipient
                            , EmailBody);
            }
            catch (Exception ex)
            {
                string ToMember = "";
                foreach (var c in m.To) { ToMember += c.Address + ";"; }
                m.To.Clear();
                m.To.Add(new MailAddress("daniel.chou@onelab.tw"));
                m.Subject = "[PayeeMail Error]" + m.Subject;
                m.Body = ex.ToString() + "<br />[To:]" + ToMember + EmailBody;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadExcel2DataTable();
        }




        private void LoadExcel2DataTable()
        {
            bool flag;
            string 
                msgRebuild = "Keep records and update it?";

            DialogResult result = MessageBox.Show(msgRebuild, "Import Records", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                Finace_Excel EEX;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    EEX = new Finace_Excel();
                    EEX.Rpt_ExcelApp = new Excel.ApplicationClass();
                    EEX.Rpt_ReportExportFileName = openFileDialog1.FileName;
                    flag = EEX.OpenExcelFile_II(SheetName);

                    if (flag)
                    {
                        dt=EEX.readData();
                        dt.DefaultView.Sort = "Payee asc";
                        this.EmpGrid.DataSource = dt;
                        this.EmpGrid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        this.EmpGrid.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        this.EmpGrid.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        this.EmpGrid.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                        MessageBox.Show("Created.", "Import Records", MessageBoxButtons.OK);
                    }
                    EEX.close();
                }
            }
        }

        int iIsHtml = 1;

        //Switch html editor and priviewer.
        private void BtnPreview_Click(object sender, EventArgs e)
        {
            iIsHtml *= -1;
            SaveTemplateFile(TemplatePath, false);
            ReloadTemplateParser();

            this.txtContent.Visible = false;
            this.webBrowser1.Visible = false;
            if (iIsHtml > 0) { 
                this.webBrowser1.Visible = true;
                this.BtnPreview.Text = "編輯";
            }
            else { 
                this.txtContent.Visible = true;
                this.BtnPreview.Text = "檢視";
            }
            
        }

        private void SaveTemplateFile(string path, bool IsSaveAs)
        {
            try
            {
                TextWriter tw = new StreamWriter(path, IsSaveAs);
                tw.Write(this.txtContent.Text.Trim());
                tw.Close();
                if (IsSaveAs) MessageBox.Show("Email樣版已另存成功!");
            }
            catch (Exception ex) { 
                
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            this.SaveAsDialog.DefaultExt = "htm";
            string[] arrs = TemplatePath.Split(new string[] { "\\" }, StringSplitOptions.None);
            for (int i = 0; i < arrs.Length - 1; i++) { NewTemplatePath += arrs[i]+"\\"; }
            this.SaveAsDialog.InitialDirectory = NewTemplatePath;
            this.SaveAsDialog.Filter = "Email樣版|*.htm";
            this.SaveAsDialog.ShowDialog();
        }

        /// <summary>
        /// Save as a new email template file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string NewPath = SaveAsDialog.FileName;
            SaveTemplateFile(NewPath, true);
            TemplatePath = NewPath;
            ReloadTemplateParser();
        }

        private void BtnOpenTemplate_Click(object sender, EventArgs e)
        {
            this.OpenEmailTemplate.DefaultExt = "htm";
            string[] arrs = TemplatePath.Split(new string[] { "\\" }, StringSplitOptions.None);
            for (int i = 0; i < arrs.Length - 1; i++) { NewTemplatePath += arrs[i] + "\\"; }
            this.OpenEmailTemplate.InitialDirectory = NewTemplatePath;
            this.OpenEmailTemplate.Filter = "Email樣版|*.htm";
            this.OpenEmailTemplate.ShowDialog();
        }

        private void OpenEmailTemplate_FileOk(object sender, CancelEventArgs e)
        {
            string NewPath=OpenEmailTemplate.FileName;
            TemplatePath = NewPath;
            ReloadTemplateParser();
        }

        private void BtnSaveAppConfig_Click(object sender, EventArgs e)
        {
            //Open App.Config of executable,  You need to add reference to System.Configuration namespace to make this code running.
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            // Add an Application Setting.
            KeyValueConfigurationCollection settings = config.AppSettings.Settings;
            settings.Remove("EmailSender");
            settings.Add("EmailSender", this.txtMailer.Text.Trim());
            settings.Remove("Send2Tester");
            settings.Add("Send2Tester", this.txtMailTester.Text.Trim());
            settings.Remove("SheetName");
            settings.Add("SheetName", this.txtExcelSheetName.Text.Trim());

            config.Save(ConfigurationSaveMode.Modified); // Save the configuration file.
            // Force a reload of a changed section.
            ConfigurationManager.RefreshSection("appSettings");
        }


        #region Ugly code for first version.

        //        private void BindingEmailContent() {
        //            Regex regex = new Regex(@"(\r\n)");
        //            string EmailBody = ""
        //                        , CssStyle = @"<style>
        //                                        p#all { font-family: Arial; font-size:0.75em; }
        //                                        table#tb1 tr td { font-family: Arial; padding:3px 20px; margin:0; font-size:0.7em; font-weight:bold; }
        //                                        .tt { background-color:#eee; text-align:center; }
        //                                        .center { text-align:center; }
        //                                        .left { text-align:left;}
        //                                        .right { text-align:right;}
        //                                        .red { color:red; }
        //                                        .blue { color:blue; }
        //                                   </style>"
        //                        , EmailBodyTemplate = regex.Replace(txtContent.Text.Trim(), "<br />")
        //                        , Payee, Email, Amount, SumAmount, Dscr, SendDate;
        //            string NextPayee = ""
        //                    , HtmlTable = ""
        //                    , HtmlTitleTemplate = "<table id='tb1' border='1' cellspacing='0' cellpadding='0'><tr><td class='tt'>Payee</td><td class='tt'>Amount</td><td class='tt'>Description of Payment</td></tr>"
        //                    , HtmlBodyTemplate = "<tr><td class='center'>{0}</td><td class='right'>{1}</td><td class='left'>{2}</td></tr>"
        //                    , HtmlBodySubtotalTemplate = "<tr><td class='center'>Subtotal</td><td class='right'>{0}</td><td class='left'></td></tr>"
        //                    , HtmlFooterTemplate = "</table>";
        //            decimal dAmount = 0, dSumAmount = 0;
        //            int k = 0;
        //            SendDate = DateTime.Now.ToString("MMM d,yyyy", new CultureInfo("en-US"));

        //            try
        //            {
        //                dv = dt.DefaultView;
        //                dv.Sort = ConfigurationSettings.AppSettings["DataGridViewSort"].ToString();
        //                dt = dv.ToTable();

        //                for (int i = 0; i <= dt.Rows.Count - 1; i++)
        //                {
        //                    NextPayee = (i == dt.Rows.Count - 1) ? "" : dt.Rows[i + 1][0].ToString();
        //                    Payee = dt.Rows[i][0].ToString();
        //                    Email = dt.Rows[i][1].ToString();
        //                    dAmount = System.Convert.ToDecimal(dt.Rows[i][2].ToString());
        //                    Amount = dAmount.ToString("N0");
        //                    Dscr = dt.Rows[i][3].ToString();
        //                    //--- Replace varaibles from DataRows. ---
        //                    HtmlTable += (k == 0) ? HtmlTitleTemplate : "";
        //                    HtmlTable += string.Format(HtmlBodyTemplate, Payee, Amount, Dscr);
        //                    dSumAmount += dAmount;

        //                    if (Payee == NextPayee)
        //                    {
        //                        k += 1;
        //                    }
        //                    else
        //                    {
        //                        SumAmount = dSumAmount.ToString("N0");
        //                        HtmlTable += string.Format(HtmlBodySubtotalTemplate, SumAmount);
        //                        HtmlTable += HtmlFooterTemplate;
        //                        EmailBody = EmailBodyTemplate
        //                                        .Replace("{name}", "<b class=blue>" + Payee + "</b>")
        //                                        .Replace("{date}", "<b class=red>" + SendDate + "</b>")
        //                                        .Replace("{money}", "<b class=red>" + SumAmount + "</b>")
        //                                        .Replace("{list}", HtmlTable);
        //                        paySEND(txtMailer.Text.Trim(), CssStyle + "<p id=all>" + EmailBody + "</p>");
        //                        // --- Reset index and summation amount.---
        //                        k = 0;
        //                        dSumAmount = 0;
        //                        HtmlTable = "";
        //                    }
        //                }

        //                //MessageBox.Show("test", "Send");
        //            }
        //            catch (Exception ex)
        //            {
        //                MessageBox.Show(ex.Message, "error");
        //            }
        //        }

        #endregion


    }
}
