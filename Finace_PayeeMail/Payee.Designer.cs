﻿namespace Finace_PayeeMail {
	partial class Payee {
		/// <summary>
		/// 設計工具所需的變數。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清除任何使用中的資源。
		/// </summary>
		/// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 設計工具產生的程式碼

		/// <summary>
		/// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
		/// 修改這個方法的內容。
		/// </summary>
		private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Payee));
            this.EmpGrid = new System.Windows.Forms.DataGridView();
            this.ckID = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.mytab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtSendDate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnOpenTemplate = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.BtnPreview = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnSendMailer = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SaveAsDialog = new System.Windows.Forms.SaveFileDialog();
            this.OpenEmailTemplate = new System.Windows.Forms.OpenFileDialog();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtMailer = new System.Windows.Forms.TextBox();
            this.BtnSaveAppConfig = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtExcelSheetName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LabMailServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMailTester = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.EmpGrid)).BeginInit();
            this.mytab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // EmpGrid
            // 
            this.EmpGrid.AllowUserToAddRows = false;
            this.EmpGrid.AllowUserToDeleteRows = false;
            this.EmpGrid.AllowUserToOrderColumns = true;
            this.EmpGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.EmpGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.EmpGrid.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.EmpGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.EmpGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EmpGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ckID});
            this.EmpGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.EmpGrid.Location = new System.Drawing.Point(7, 6);
            this.EmpGrid.Name = "EmpGrid";
            this.EmpGrid.RowHeadersVisible = false;
            this.EmpGrid.RowHeadersWidth = 25;
            this.EmpGrid.RowTemplate.Height = 24;
            this.EmpGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.EmpGrid.Size = new System.Drawing.Size(648, 509);
            this.EmpGrid.TabIndex = 48;
            // 
            // ckID
            // 
            this.ckID.HeaderText = "";
            this.ckID.Name = "ckID";
            // 
            // mytab
            // 
            this.mytab.Controls.Add(this.tabPage1);
            this.mytab.Controls.Add(this.tabPage2);
            this.mytab.Controls.Add(this.tabPage3);
            this.mytab.Location = new System.Drawing.Point(12, 12);
            this.mytab.Name = "mytab";
            this.mytab.SelectedIndex = 0;
            this.mytab.Size = new System.Drawing.Size(773, 547);
            this.mytab.TabIndex = 49;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.EmpGrid);
            this.tabPage1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(765, 521);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "檢視EXCEL";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(661, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 23);
            this.button1.TabIndex = 49;
            this.button1.Text = "匯入Excel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtSendDate);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.btnSendMailer);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.txtSubject);
            this.tabPage2.Controls.Add(this.webBrowser1);
            this.tabPage2.Controls.Add(this.txtContent);
            this.tabPage2.Controls.Add(this.LabMailServer);
            this.tabPage2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(765, 521);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "信件樣版內容";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtSendDate
            // 
            this.txtSendDate.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSendDate.Location = new System.Drawing.Point(112, 47);
            this.txtSendDate.Name = "txtSendDate";
            this.txtSendDate.Size = new System.Drawing.Size(115, 26);
            this.txtSendDate.TabIndex = 62;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(15, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 12);
            this.label1.TabIndex = 61;
            this.label1.Text = "{{SendDate}}：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BtnOpenTemplate);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.BtnPreview);
            this.groupBox1.Location = new System.Drawing.Point(8, 84);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(218, 52);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Email 樣版";
            // 
            // BtnOpenTemplate
            // 
            this.BtnOpenTemplate.Location = new System.Drawing.Point(9, 17);
            this.BtnOpenTemplate.Name = "BtnOpenTemplate";
            this.BtnOpenTemplate.Size = new System.Drawing.Size(52, 28);
            this.BtnOpenTemplate.TabIndex = 59;
            this.BtnOpenTemplate.Text = "開啟";
            this.BtnOpenTemplate.UseVisualStyleBackColor = true;
            this.BtnOpenTemplate.Click += new System.EventHandler(this.BtnOpenTemplate_Click);
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(126, 17);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(82, 28);
            this.button2.TabIndex = 58;
            this.button2.Text = "另存新檔";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BtnPreview
            // 
            this.BtnPreview.Location = new System.Drawing.Point(67, 17);
            this.BtnPreview.Name = "BtnPreview";
            this.BtnPreview.Size = new System.Drawing.Size(52, 28);
            this.BtnPreview.TabIndex = 56;
            this.BtnPreview.Text = "編輯";
            this.BtnPreview.UseVisualStyleBackColor = true;
            this.BtnPreview.Click += new System.EventHandler(this.BtnPreview_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Gray;
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button3.Location = new System.Drawing.Point(606, 91);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(143, 45);
            this.button3.TabIndex = 55;
            this.button3.Text = "按照列表寄信!";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnSendMailer
            // 
            this.btnSendMailer.Location = new System.Drawing.Point(494, 91);
            this.btnSendMailer.Name = "btnSendMailer";
            this.btnSendMailer.Size = new System.Drawing.Size(106, 45);
            this.btnSendMailer.TabIndex = 51;
            this.btnSendMailer.Text = "[Test] 寄給測試者";
            this.btnSendMailer.UseVisualStyleBackColor = true;
            this.btnSendMailer.Click += new System.EventHandler(this.btnSendMailer_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(15, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "信件標題：";
            // 
            // txtSubject
            // 
            this.txtSubject.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubject.Location = new System.Drawing.Point(112, 12);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(637, 26);
            this.txtSubject.TabIndex = 32;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(8, 142);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(722, 373);
            this.webBrowser1.TabIndex = 57;
            // 
            // txtContent
            // 
            this.txtContent.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContent.Location = new System.Drawing.Point(7, 142);
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtContent.Size = new System.Drawing.Size(742, 373);
            this.txtContent.TabIndex = 31;
            this.txtContent.Text = resources.GetString("txtContent.Text");
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // SaveAsDialog
            // 
            this.SaveAsDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // OpenEmailTemplate
            // 
            this.OpenEmailTemplate.FileName = "openFileDialog2";
            this.OpenEmailTemplate.FileOk += new System.ComponentModel.CancelEventHandler(this.OpenEmailTemplate_FileOk);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.txtMailTester);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.txtExcelSheetName);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.BtnSaveAppConfig);
            this.tabPage3.Controls.Add(this.txtMailer);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(765, 521);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "進階設定";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtMailer
            // 
            this.txtMailer.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMailer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtMailer.Location = new System.Drawing.Point(131, 22);
            this.txtMailer.Name = "txtMailer";
            this.txtMailer.Size = new System.Drawing.Size(198, 26);
            this.txtMailer.TabIndex = 55;
            // 
            // BtnSaveAppConfig
            // 
            this.BtnSaveAppConfig.Location = new System.Drawing.Point(131, 135);
            this.BtnSaveAppConfig.Name = "BtnSaveAppConfig";
            this.BtnSaveAppConfig.Size = new System.Drawing.Size(75, 23);
            this.BtnSaveAppConfig.TabIndex = 56;
            this.BtnSaveAppConfig.Text = "儲存";
            this.BtnSaveAppConfig.UseVisualStyleBackColor = true;
            this.BtnSaveAppConfig.Click += new System.EventHandler(this.BtnSaveAppConfig_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 12);
            this.label3.TabIndex = 57;
            this.label3.Text = "預設Excel標籤：";
            // 
            // txtExcelSheetName
            // 
            this.txtExcelSheetName.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExcelSheetName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtExcelSheetName.Location = new System.Drawing.Point(131, 87);
            this.txtExcelSheetName.Name = "txtExcelSheetName";
            this.txtExcelSheetName.Size = new System.Drawing.Size(199, 26);
            this.txtExcelSheetName.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 59;
            this.label4.Text = "寄件者：";
            // 
            // LabMailServer
            // 
            this.LabMailServer.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabMailServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.LabMailServer.Location = new System.Drawing.Point(112, 12);
            this.LabMailServer.Name = "LabMailServer";
            this.LabMailServer.ReadOnly = true;
            this.LabMailServer.Size = new System.Drawing.Size(127, 26);
            this.LabMailServer.TabIndex = 63;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 61;
            this.label5.Text = "測試者：";
            // 
            // txtMailTester
            // 
            this.txtMailTester.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMailTester.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtMailTester.Location = new System.Drawing.Point(131, 54);
            this.txtMailTester.Name = "txtMailTester";
            this.txtMailTester.Size = new System.Drawing.Size(198, 26);
            this.txtMailTester.TabIndex = 60;
            // 
            // Payee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 571);
            this.Controls.Add(this.mytab);
            this.Name = "Payee";
            this.Text = "Payee";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EmpGrid)).EndInit();
            this.mytab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView EmpGrid;
		private System.Windows.Forms.TabControl mytab;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TextBox txtContent;
		private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Button btnSendMailer;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button BtnPreview;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ckID;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SaveFileDialog SaveAsDialog;
        private System.Windows.Forms.OpenFileDialog OpenEmailTemplate;
        private System.Windows.Forms.Button BtnOpenTemplate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSendDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtMailer;
        private System.Windows.Forms.Button BtnSaveAppConfig;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtExcelSheetName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox LabMailServer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMailTester;

	}
}

